package com.danilodequeiroz.easyinvestchallenge.app

import android.app.Application
import com.danilodequeiroz.easyinvestchallenge.BuildConfig
import com.danilodequeiroz.easyinvestchallenge.common.util.isRoboUnitTest
import com.danilodequeiroz.easyinvestchallenge.investmentform.di.moduleInvestFormModule
import com.danilodequeiroz.easyinvestchallenge.investmentform.di.moduleNetworkRepository
import com.facebook.stetho.Stetho
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext
import org.koin.core.context.startKoin


class CustomApplication : Application() {


    override fun onCreate() {
        super.onCreate()
        if(GlobalContext.getOrNull() == null) {
            startKoin {
                // use AndroidLogger as Koin Logger - default Level.INFO
                androidLogger()
                // use the Android context given there
                androidContext(this@CustomApplication)
                // load properties from assets/koin.properties file
                androidFileProperties()
                // module list
                modules(
                    listOf(
                        moduleInvestFormModule,
                        moduleNetworkRepository
                    )
                )
            }
        }
        if (BuildConfig.DEBUG && !isRoboUnitTest()) {
            Stetho.initializeWithDefaults(this)
        }
    }




}
