package com.danilodequeiroz.easyinvestchallenge.common

import android.app.Activity
import android.content.Intent
import androidx.test.core.app.ActivityScenario

inline fun <reified T:Activity> launchActivity()= ActivityScenario.launch(T::class.java)
//inline fun <reified T:Activity> launchActivityWithIntent(intent: Intent)= ActivityScenario.launch(intent)