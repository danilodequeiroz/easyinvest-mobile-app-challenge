package com.danilodequeiroz.easyinvestchallenge.activity

import android.content.Intent
import android.os.Build
import android.widget.TextView
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.danilodequeiroz.easyinvestchallenge.R
import com.danilodequeiroz.easyinvestchallenge.common.INVESTMENT_MODEL_PARAM
import com.danilodequeiroz.easyinvestchallenge.common.launchActivity
import com.danilodequeiroz.easyinvestchallenge.investmentform.domain.toViewModel
import com.danilodequeiroz.easyinvestchallenge.investmentresult.ui.InvestmentResultActivity
import com.danilodequeiroz.restapi.repository.mock.Mocks
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric.buildActivity
import org.robolectric.annotation.Config


@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class InvestmentResultActivityTest {



    @Test
    fun passingNoDate_activityOpen_shouldSeeInvalidParamError() {
        val scenario = launchActivity<InvestmentResultActivity>()
        val controller = buildActivity<InvestmentResultActivity>(InvestmentResultActivity::class.java).setup()

        assertTrue(controller.get().didSucceedDialog.isShowing)
        assertTrue(controller.get().findViewById<TextView>(R.id.tv_1_invested_amount_value).text == "R$ 10.000,00")
        scenario.moveToState(Lifecycle.State.DESTROYED)
    }

    @Test
    fun passingModel_activityOpen_shouldDisplayResults() {

        val scenario = launchActivity<InvestmentResultActivity>()

        val intent = Intent(ApplicationProvider.getApplicationContext<InvestmentResultActivity>(),InvestmentResultActivity::class.java)
        intent.putExtra(INVESTMENT_MODEL_PARAM,Mocks().investmentObject.toViewModel(123.0))
        ActivityScenario.launch<InvestmentResultActivity>(intent)

        val controller = buildActivity<InvestmentResultActivity>(InvestmentResultActivity::class.java)
            .newIntent(intent)
            .setup()

        assertTrue(controller.get().didSucceedDialog.isShowing)
        assertTrue(controller.get().findViewById<TextView>(R.id.tv_1_invested_amount_value).text == "R$ 10.000,00")
        scenario.moveToState(Lifecycle.State.DESTROYED)
    }

}