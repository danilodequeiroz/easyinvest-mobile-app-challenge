package com.danilodequeiroz.easyinvestchallenge.activity

import android.os.Build
import android.widget.EditText
import androidx.lifecycle.Lifecycle
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.danilodequeiroz.easyinvestchallenge.common.ext.toEditable
import com.danilodequeiroz.easyinvestchallenge.common.launchActivity
import com.danilodequeiroz.easyinvestchallenge.investmentform.R
import com.danilodequeiroz.easyinvestchallenge.investmentform.ui.InvestmentActivity
import com.google.android.material.button.MaterialButton
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric.buildActivity
import org.robolectric.annotation.Config
import org.robolectric.shadows.ShadowSystemClock
import org.robolectric.shadows.ShadowToast

@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class InvestmentActivityTest {


    @Test
    fun leftTwoFieldsEmpty_clickSimulate_invalidData() {
        val scenario = launchActivity<InvestmentActivity>()
        val controller = buildActivity<InvestmentActivity>(InvestmentActivity::class.java).setup()

        controller.get().findViewById<EditText>(R.id.et_investment_amount).text =
            "200000".toEditable()
        controller.get().findViewById<MaterialButton>(R.id.bt_simulate).performClick()

        assertEquals(
            controller.get().findViewById<EditText>(R.id.et_investment_amount).text.toString(),
            "R$ 2.000,00"
        )
        assertEquals("Dados inválidos, verifique os campos.", ShadowToast.getTextOfLatestToast())
        scenario.moveToState(Lifecycle.State.DESTROYED)
    }

    @Test
    fun leftDateFieldsEmpty_clickSimulate_invalidData() {
        val scenario = launchActivity<InvestmentActivity>()
        val controller = buildActivity<InvestmentActivity>(InvestmentActivity::class.java).setup()

        controller.get().findViewById<EditText>(R.id.et_investment_amount).text =
            "200000".toEditable()

        controller.get().findViewById<EditText>(R.id.et_cdi_index_title).text =
            "3%".toEditable()
        controller.get().findViewById<MaterialButton>(R.id.bt_simulate).performClick()

        assertEquals(
            controller.get().findViewById<EditText>(R.id.et_investment_amount).text.toString(),
            "R$ 2.000,00"
        )
        assertEquals(
            controller.get().findViewById<EditText>(R.id.et_cdi_index_title).text.toString(),
            "3%"
        )
        assertEquals("Dados inválidos, verifique os campos.", ShadowToast.getTextOfLatestToast())
        scenario.moveToState(Lifecycle.State.DESTROYED)
    }


    @Test
    fun filledForm_clickSimulate_dontShowValidationToast() {
        val scenario = launchActivity<InvestmentActivity>()
        val controller = buildActivity<InvestmentActivity>(InvestmentActivity::class.java).setup()
        controller.get().findViewById<EditText>(R.id.et_investment_amount).text =
            "200000".toEditable()
        controller.get().findViewById<EditText>(R.id.et_investment_maturity_date).text =
            "29/10/2021".toEditable()
        controller.get().findViewById<EditText>(R.id.et_cdi_index_title).text =
            "3%".toEditable()

        controller.get().findViewById<MaterialButton>(R.id.bt_simulate).performClick()

        assertEquals(
            controller.get().findViewById<EditText>(R.id.et_investment_amount).text.toString(),
            "R$ 2.000,00"
        )
        assertEquals(
            controller.get().findViewById<EditText>(R.id.et_investment_maturity_date).text.toString(),
            "29/10/2021"
        )
        assertEquals(
            controller.get().findViewById<EditText>(R.id.et_cdi_index_title).text.toString(),
            "3%"
        )
        assertEquals(null, ShadowToast.getTextOfLatestToast())
        scenario.moveToState(Lifecycle.State.DESTROYED)
    }
}