package com.danilodequeiroz.easyinvestchallenge

import android.content.Context
import android.os.Build
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.danilodequeiroz.easyinvestchallenge.app.CustomApplication
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class ExampleUnitTest {


    var context : Context? = null
    @Before
    fun initContext() {
        context = ApplicationProvider.getApplicationContext()
    }

    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
    @Test
    fun appContext_verify_AppName() {
        val appContext = getApplicationContext<CustomApplication>()
        assertEquals("com.danilodequeiroz.easyinvestchallenge.debug", appContext.packageName)
    }
}
