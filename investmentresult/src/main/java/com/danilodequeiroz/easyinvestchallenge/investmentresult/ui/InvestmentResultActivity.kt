package com.danilodequeiroz.easyinvestchallenge.investmentresult.ui

import android.content.DialogInterface
import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.danilodequeiroz.easyinvestchallenge.common.INVESTMENT_MODEL_PARAM
import com.danilodequeiroz.easyinvestchallenge.common.ext.toHtml
import com.danilodequeiroz.easyinvestchallenge.common.interchangeable_viewmodel.InvestmentViewModel
import com.danilodequeiroz.easyinvestchallenge.common.investmentresult.R
import kotlinx.android.synthetic.main.activity_investment_result.*


class InvestmentResultActivity : AppCompatActivity() {


    val didSucceedDialog by lazy { AlertDialog.Builder(this).create() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_investment_result)
        setupToolbar()

        intent.getParcelableExtra<InvestmentViewModel>(INVESTMENT_MODEL_PARAM)?.let {
            bindLabels(it)
            bt_simulate.setOnClickListener { finish() }
        } ?: kotlin.run {
            setFeedbackDialogAndShow(R.string.ops, R.string.invalid_param)
        }
    }

    private fun bindLabels(it: InvestmentViewModel) {
        tv_gross_amount_large.text = it.grossAmount
        tv_main_gross_amount_profit.text = getString(R.string.lbl_simulation_gross_amount_profit, it.grossAmountProfit).toHtml()
        tv_1_invested_amount_value.text = it.investedAmount
        tv_2_gross_amount_value.text = it.grossAmount
        tv_3_gross_amount_profit_value.text = it.grossAmountProfit
        tv_4_taxesrate_value.text = it.taxesRate
        tv_5_net_amount_value.text = it.netAmountProfit
        tv_6_maturity_date_value.text = it.maturityDate
        tv_7_maturity_total_days_value.text = "${it.maturityTotalDays}"
        tv_8_monthly_gross_rate_profit_value.text = it.monthlyGrossRateProfit
        tv_9_cdi_index_value.text = it.cdiIndex
        tv_10_yearly_interest_rate_value.text = it.yearlyInterestRate
        tv_11_rate_profit_value.text = it.rateProfit
    }


    private fun setFeedbackDialogAndShow(@StringRes title: Int, @StringRes resMessage: Int, customMessage: String? = null) {
        didSucceedDialog.setTitle(getString(title))
        val message = if (customMessage.isNullOrBlank()) getString(resMessage) else customMessage
        didSucceedDialog.setMessage(message)
        didSucceedDialog.setButton(
            DialogInterface.BUTTON_POSITIVE,
            getString(R.string.lbl_ok)
        ) { dialog, _ -> dialog.dismiss() }
        didSucceedDialog.show()
    }


    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.setTitleTextColor(
            ContextCompat.getColor(
                applicationContext,
                R.color.material_white
            )
        )

        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override  fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home ->{
                onBackPressed()
                return false
            }
        }
        return super.onOptionsItemSelected(item)
    }


}
