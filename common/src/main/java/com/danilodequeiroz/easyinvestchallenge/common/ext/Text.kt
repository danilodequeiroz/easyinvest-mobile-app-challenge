package com.danilodequeiroz.easyinvestchallenge.common.ext

import android.os.Build
import android.text.Editable
import android.text.Html
import android.text.Spanned

fun String.toEditable() = Editable.Factory.getInstance().newEditable(this)


fun String.toHtml(): Spanned {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
    else
        Html.fromHtml(this)
}