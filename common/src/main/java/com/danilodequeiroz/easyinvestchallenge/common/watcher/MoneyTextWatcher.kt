package com.danilodequeiroz.easyinvestchallenge.common.watcher

import android.annotation.SuppressLint
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import com.danilodequeiroz.easyinvestchallenge.common.ext.toEditable
import java.lang.ref.WeakReference
import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*

@SuppressLint("SetTextI18n")
class MoneyTextWatcher(editText: EditText, locale: Locale?) : TextWatcher {
    private val editTextWeakReference: WeakReference<EditText> = WeakReference(editText)
    private val locale: Locale = locale ?: Locale.getDefault()

     init {
        val moneySymbol = NumberFormat.getCurrencyInstance(this.locale).currency?.symbol
        editText.text = "$moneySymbol 0,00".toEditable()
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

    }

    override fun afterTextChanged(editable: Editable) {
        val editText = editTextWeakReference.get() ?: return
        editText.removeTextChangedListener(this)

        val parsed = parseToBigDecimal(editable.toString(), locale)
        val formatted = NumberFormat.getCurrencyInstance(locale).format(parsed)
        // NumberFormat.getNumberInstance(locale).format(parsed); // sem o simbolo de moeda

        editText.setText(formatted)
        editText.setSelection(formatted.length)
        editText.addTextChangedListener(this)
    }

    private fun parseToBigDecimal(value: String, locale: Locale): BigDecimal {
        val replaceable =
            String.format("[%s,.\\s]", NumberFormat.getCurrencyInstance(locale).currency?.symbol)

        var cleanString = value.replace(replaceable.toRegex(), "")

        cleanString = if(cleanString.isBlank()) "0.0" else cleanString

        return BigDecimal(cleanString).setScale(
            2, BigDecimal.ROUND_FLOOR
        ).divide(
            BigDecimal(100), BigDecimal.ROUND_FLOOR
        )
    }
}