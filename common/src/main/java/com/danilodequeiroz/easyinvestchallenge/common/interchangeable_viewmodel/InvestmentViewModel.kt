package com.danilodequeiroz.easyinvestchallenge.common.interchangeable_viewmodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class InvestmentViewModel(

    val investedAmount: String? = null,             // valor aplicado inicialmente
    val grossAmount: String? = null,                // valor bruto do investimento
    val grossAmountProfit: String? = null,          // valor do rendimento
    val taxesRate: String? = null,                  // IR sobre inv.
    val netAmountProfit: String? = null,                  // valor liquid investmento


    val maturityDate: String? = null,               // Data de vencimento
    val maturityTotalDays: Int? = null,             // Dias corridos
    val monthlyGrossRateProfit: String? = null,     // Rendimento mensal
    val cdiIndex: String? = null,                   // Percental do CDI do investmento
    val yearlyInterestRate: String? = null,         // Rentabilidde anual
    val rateProfit: String? = null                  // Rentabilidde do período


) : Parcelable


