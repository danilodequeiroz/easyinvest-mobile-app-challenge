package com.danilodequeiroz.easyinvestchallenge.common

const val SCHEDULER_SUBSCRIBE_ON_IO = "SCHEDULER_SUBSCRIBE_ON_IO"
const val SCHEDULER_OBSERVE_ON_MAIN_THREAD = "SCHEDULER_OBSERVE_ON_MAIN_THREAD"
const val GSON = "GSON"
const val OKHTTP3_CLIENT = "OKHTTP3_CLIENT"
const val RETROFIT = "RETROFIT"
const val DATE_PATTERN = "dd/MM/yyyy"
const val API_DATE_PATTERN = "yyyy-MM-dd"


const val INVESTMENT_MODEL_PARAM = "INVESTMENT_MODEL_PARAM"


