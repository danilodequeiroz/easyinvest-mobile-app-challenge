package com.danilodequeiroz.easyinvestchallenge.common

import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*

object MoneyUtils {

    fun formattedValue(value: Long?): String {
        return formatValue(
            value
        )
    }

    fun formattedValueToAPI(value: String): Long {
        val reEvaluate = value.replace("\\D+".toRegex(), "")
        val money = BigDecimal(reEvaluate)
        return money.toLong()
    }
    
    private fun formatValue(value: Long?): String {
        return if (value == null) {
            "-"
        } else {
            val numberFormat = NumberFormat.getCurrencyInstance(Locale("pt", "BR"))
            return numberFormat.format(BigDecimal(value).movePointLeft(2))
        }
    }
}