package com.danilodequeiroz.easyinvestchallenge.common.exception

import java.io.IOException

class UnknowException : IOException("Erro desconhecido")