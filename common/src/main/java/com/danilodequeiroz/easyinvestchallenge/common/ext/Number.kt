package com.danilodequeiroz.easyinvestchallenge.common.ext

import java.text.DecimalFormat
import java.text.MessageFormat
import java.text.NumberFormat
import java.util.*

fun Number.toBRLMoneyString(): String {
    return "R$ %.2f".format(Locale("pt", "BR"), this.toDouble())
}

fun Double.toPercentString(): String {
    val decimalSeparator = (NumberFormat.getPercentInstance(
        Locale(
            "pt",
            "BR"
        )
    ) as DecimalFormat).decimalFormatSymbols.decimalSeparator.toString()
    val format = MessageFormat.format("{0,number,#.##%}", this/100)
    return format.replace(".", decimalSeparator)
}
