package com.danilodequeiroz.easyinvestchallenge.common.watcher

import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.widget.EditText
import androidx.core.text.toSpannable
import com.danilodequeiroz.easyinvestchallenge.common.ext.toEditable
import java.lang.ref.WeakReference
import java.util.*

class PercentTextWatcher(editText: EditText) : TextWatcher {
    private val editTextWeakReference: WeakReference<EditText> = WeakReference(editText)

    init {
        editText.text = "0%".toEditable()
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

    }

    override fun afterTextChanged(editable: Editable) {
        val editText = editTextWeakReference.get() ?: return
        editText.removeTextChangedListener(this)

        val currentSelection = if (editText.selectionStart > 0) editable.substring(
            editText.selectionStart - 1,
            editText.selectionEnd
        ) else ""

        var edtbleEdited = editable.replace(Regex.fromLiteral("."), ",").toEditable()
        edtbleEdited = edtbleEdited.replace(Regex.fromLiteral("%"), "").toEditable()

        val commaAndDotCount =
            edtbleEdited.count { ",".contains(it) } + edtbleEdited.count { ".".contains(it) }

        if ((currentSelection.contains(",") || currentSelection.contains(".")) && commaAndDotCount > 1) {
            edtbleEdited.delete(editText.selectionStart - 1, editText.selectionEnd)
        }

        if (edtbleEdited.toString() == ",")  {
            edtbleEdited = SpannableStringBuilder("0,")
        }

        val percent = if (edtbleEdited.isNullOrBlank()) "0%" else "${edtbleEdited.replaceFirst(Regex("^0+(?=\\d+$)"), "")}%"
        editText.setText(percent)
        editText.setSelection(percent.length -1)
        editText.addTextChangedListener(this)
    }


}