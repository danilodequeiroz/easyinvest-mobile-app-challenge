package com.danilodequeiroz.easyinvestchallenge.common.ext

import com.danilodequeiroz.easyinvestchallenge.common.exception.UnknowException
import java.io.IOException
import java.net.SocketException
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException

fun Throwable?.isNetworkError() = when (this) {
    is UnknowException -> false
    is TimeoutException -> true
    is SocketException -> true
    is UnknownHostException -> true
    is IOException -> false
    else -> false

}