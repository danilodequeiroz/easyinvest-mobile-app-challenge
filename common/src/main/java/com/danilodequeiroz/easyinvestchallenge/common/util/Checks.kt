package com.danilodequeiroz.easyinvestchallenge.common.util

import android.os.Build

fun isRoboUnitTest(): Boolean = "robolectric" == Build.FINGERPRINT
