package com.danilodequeiroz.easyinvestchallenge.investmentform.viewmodel_interactor

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.danilodequeiroz.easyinvestchallenge.common.mock
import com.danilodequeiroz.easyinvestchallenge.common.whenever
import com.danilodequeiroz.easyinvestchallenge.investmentform.domain.InvestmentFormInteractor
import com.danilodequeiroz.easyinvestchallenge.investmentform.domain.toViewModel
import com.danilodequeiroz.easyinvestchallenge.investmentform.presentation.*
import com.danilodequeiroz.easyinvestchallenge.investmentform.viewmodel.screen.InvestFormViewModel
import com.danilodequeiroz.restapi.model.Investment
import com.danilodequeiroz.restapi.repository.InvestmentFormRepository
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.apache.commons.validator.GenericValidator
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers.anyDouble
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.mockito.Mockito.never
import org.mockito.Mockito.verify


class InvestmentFormViewModelInteractorUnitTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    val repository =
        mock<InvestmentFormRepository>()

    val observerState =
        mock<Observer<InvestmentFormState>>()

    val interactor by lazy {
        InvestmentFormInteractor(repository)
    }

    val viewmodel by lazy {
        InvestFormViewModel(
            interactor,
            Schedulers.trampoline(),
            Schedulers.trampoline()
        )
    }

    @Before
    fun initTest() = Mockito.reset(observerState)


    @Test
    fun viewmodelInstantiate_nothingHappens_stateKeepsInitial() {
        val argumentCaptor = ArgumentCaptor.forClass(InvestmentFormState::class.java)
        val expectedInitialState = InitialInvestmentFormState()

        viewmodel.stateInvestmentForm.observeForever(observerState)
        viewmodel.initInvestmentForm()

        verify(repository, never()).getInvestmentSimulation(anyDouble(), anyString(), anyDouble())
        argumentCaptor.run {
            verify(observerState, Mockito.times(1)).onChanged(this.capture())

            val (initialState) = allValues

            assertEquals(initialState, expectedInitialState)
        }
    }


    @Test
    fun inputFilledOutRandom_applySimulation_stateChanges() {
        val invested = 1000.0
        val strInvested = "R$ 1.000,00"
        val date = "28/02/2019"
        val dateApi = "2019-02-28"
        val cdiIndex = 9.6
        val strCdiIndex = "9,6%"

        val givenResponse = Investment()
        val argumentCaptor = ArgumentCaptor.forClass(InvestmentFormState::class.java)
        val expectedInitialInvestmentFormState = InitialInvestmentFormState()
        val expectedLoadingInvestmentFormState = LoadingInvestmentFormState()
        val expectedCalculatedInvestmentFormState =
            CalculatedInvestmentFormState(givenResponse.toViewModel(cdiIndex))

        whenever(repository.getInvestmentSimulation(invested, dateApi, cdiIndex))
            .thenReturn(Single.just(givenResponse))
        viewmodel.stateInvestmentForm.observeForever(observerState)
        viewmodel.initInvestmentForm()
        viewmodel.getInvestmentSimulation(strInvested, date, strCdiIndex)

        verify(repository).getInvestmentSimulation(invested, dateApi, cdiIndex)
        assertEquals(interactor.stringBRLMoneyToDouble(strInvested), invested, 0.0001)
        assertEquals(interactor.stringPercentToDouble(strCdiIndex), cdiIndex, 0.0001)
        assertTrue(GenericValidator.isDate(date, "dd/MM/yyyy", true))
        argumentCaptor.run {
            verify(observerState, Mockito.times(3)).onChanged(this.capture())
            val (initialState, loadingState, calculatedState) = allValues

            assertEquals(initialState, expectedInitialInvestmentFormState)
            assertEquals(loadingState, expectedLoadingInvestmentFormState)
            assertEquals(calculatedState, expectedCalculatedInvestmentFormState)
        }
    }


    @Test
    fun inputFilledOut_applySimulation_stateChanges() {
        val invested = 1000.0;
        val strInvested = "R$ 1.000,00"
        val date = "28/02/2019"
        val dateApi = "2019-02-28"
        val cdiIndex = 9.6;
        val strCdiIndex = "9,6%"
        val givenResponse = Investment()
        val argumentCaptor = ArgumentCaptor.forClass(InvestmentFormState::class.java)
        val expectedInitialInvestmentFormState = InitialInvestmentFormState()
        val expectedLoadingInvestmentFormState = LoadingInvestmentFormState()
        val expectedCalculatedInvestmentFormState =
            CalculatedInvestmentFormState(givenResponse.toViewModel(cdiIndex))

        whenever(repository.getInvestmentSimulation(invested, dateApi, cdiIndex))
            .thenReturn(Single.just(givenResponse))
        viewmodel.stateInvestmentForm.observeForever(observerState)
        viewmodel.initInvestmentForm()
        viewmodel.getInvestmentSimulation(strInvested, date, strCdiIndex)

        verify(repository).getInvestmentSimulation(invested, dateApi, cdiIndex)
        assertEquals(interactor.stringBRLMoneyToDouble(strInvested), invested, 0.0001)
        assertEquals(interactor.stringPercentToDouble(strCdiIndex), cdiIndex, 0.0001)
        assertTrue(GenericValidator.isDate(date, "dd/MM/yyyy", true))
        argumentCaptor.run {
            verify(observerState, Mockito.times(3)).onChanged(this.capture())

            val (initialState, loadingState, calculatedState) = allValues
            assertEquals(initialState, expectedInitialInvestmentFormState)
            assertEquals(loadingState, expectedLoadingInvestmentFormState)
            assertEquals(calculatedState, expectedCalculatedInvestmentFormState)
        }
    }

    @Test
    fun wrongDate_applySimulation_stateInvalid() {
        val invested = 1000.0;
        val strInvested = "R$ 1.000,00"
        val date = "29/02/2019"
        val dateApi = "2019-02-29"
        val cdiIndex = 9.7
        val strCdiIndex = "9,7%"

        val argumentCaptor = ArgumentCaptor.forClass(InvestmentFormState::class.java)
        val expectedInitialInvestmentFormState = InitialInvestmentFormState()
        val expectedInvalidInvestmentFormState = InvalidFormState()

        viewmodel.stateInvestmentForm.observeForever(observerState)
        viewmodel.initInvestmentForm()
        viewmodel.getInvestmentSimulation(strInvested, date, strCdiIndex)

        verify(repository, never()).getInvestmentSimulation(invested, dateApi, cdiIndex)
        assertEquals(interactor.stringBRLMoneyToDouble(strInvested), invested, 0.0001)
        assertEquals(interactor.stringPercentToDouble(strCdiIndex), cdiIndex, 0.0001)
        assertFalse(GenericValidator.isDate(date, "dd/MM/yyyy", true))
        argumentCaptor.run {
            verify(observerState, Mockito.times(2)).onChanged(this.capture())

            val (initialState, invalidState) = allValues

            assertEquals(initialState, expectedInitialInvestmentFormState)
            assertEquals(invalidState, expectedInvalidInvestmentFormState)
        }
    }

    @Test
    fun emptyStringDate_applySimulation_stateInvalid() {
        val invested = 1020.26;
        val strInvested = "R$ 1.020,26"
        val date = ""
        val cdiIndex = 9.0;
        val strCdiIndex = "9%"
        val argumentCaptor = ArgumentCaptor.forClass(InvestmentFormState::class.java)
        val expectedInitialInvestmentFormState = InitialInvestmentFormState()
        val expectedInvalidInvestmentFormState = InvalidFormState()

        viewmodel.stateInvestmentForm.observeForever(observerState)
        viewmodel.initInvestmentForm()
        viewmodel.getInvestmentSimulation(strInvested, date, strCdiIndex)

        verify(repository, never()).getInvestmentSimulation(invested, date, cdiIndex)
        assertEquals(interactor.stringBRLMoneyToDouble(strInvested), invested, 0.0001)
        assertEquals(interactor.stringPercentToDouble(strCdiIndex), cdiIndex, 0.0001)
        assertFalse(GenericValidator.isDate(date, "dd/MM/yyyy", true))
        argumentCaptor.run {
            verify(observerState, Mockito.times(2)).onChanged(this.capture())

            val (initialState, invalidState) = allValues

            assertEquals(initialState, expectedInitialInvestmentFormState)
            assertEquals(invalidState, expectedInvalidInvestmentFormState)
        }
    }

    @Test
    fun zeroInvestment_applySimulation_stateInvalid() {
        val invested = 0.0;
        val strInvested = "R$ 0,0"
        val vmDate = "28/02/2029"
        val apiDate = "2029-02-28"
        val cdiIndex = 2.7;
        val strCdiIndex = "2,7%"
        val argumentCaptor = ArgumentCaptor.forClass(InvestmentFormState::class.java)
        val expectedInitialInvestmentFormState = InitialInvestmentFormState()
        val expectedInvalidInvestmentFormState = InvalidFormState()

        viewmodel.stateInvestmentForm.observeForever(observerState)
        viewmodel.initInvestmentForm()
        viewmodel.getInvestmentSimulation(strInvested, vmDate, strCdiIndex)

        verify(repository, never()).getInvestmentSimulation(invested, apiDate, cdiIndex)
        assertTrue(GenericValidator.isDate(vmDate, "dd/MM/yyyy", true))
        assertEquals(interactor.stringBRLMoneyToDouble(strInvested), invested, 0.0001)
        assertEquals(interactor.stringPercentToDouble(strCdiIndex), cdiIndex, 0.0001)
        argumentCaptor.run {
            verify(observerState, Mockito.times(2)).onChanged(this.capture())

            val (initialState, invalidState) = allValues

            assertEquals(initialState, expectedInitialInvestmentFormState)
            assertEquals(invalidState, expectedInvalidInvestmentFormState)
        }
    }

    @Test
    fun zeroCdiIndex_applySimulation_stateInvalid() {
        val invested = 9000.0;
        val strInvested = "R$ 9.000,00"
        val vmDate = "28/02/2029"
        val apiDate = "2029-02-28"
        val cdiIndex = 0.0
        val strCdiIndex = "0%"

        val argumentCaptor = ArgumentCaptor.forClass(InvestmentFormState::class.java)
        val expectedInitialInvestmentFormState = InitialInvestmentFormState()
        val expectedInvalidInvestmentFormState = InvalidFormState()

        viewmodel.stateInvestmentForm.observeForever(observerState)
        viewmodel.initInvestmentForm()
        viewmodel.getInvestmentSimulation(strInvested, vmDate, strCdiIndex)

        verify(repository, never()).getInvestmentSimulation(invested, apiDate, cdiIndex)
        assertTrue(GenericValidator.isDate(vmDate, "dd/MM/yyyy", true))
        assertEquals(interactor.stringBRLMoneyToDouble(strInvested), invested, 0.0001)
        assertEquals(interactor.stringPercentToDouble(strCdiIndex), cdiIndex, 0.0001)
        argumentCaptor.run {
            verify(observerState, Mockito.times(2)).onChanged(this.capture())

            val (initialState, invalidState) = allValues

            assertEquals(initialState, expectedInitialInvestmentFormState)
            assertEquals(invalidState, expectedInvalidInvestmentFormState)
        }
    }


}