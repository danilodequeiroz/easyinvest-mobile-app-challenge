package com.danilodequeiroz.easyinvestchallenge.investmentform.ui

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.danilodequeiroz.easyinvestchallenge.common.INVESTMENT_MODEL_PARAM
import com.danilodequeiroz.easyinvestchallenge.common.ext.toEditable
import com.danilodequeiroz.easyinvestchallenge.common.watcher.MoneyTextWatcher
import com.danilodequeiroz.easyinvestchallenge.common.watcher.PercentTextWatcher
import com.danilodequeiroz.easyinvestchallenge.investmentform.R
import com.danilodequeiroz.easyinvestchallenge.investmentform.presentation.*
import com.danilodequeiroz.easyinvestchallenge.investmentform.viewmodel.screen.InvestFormViewModel
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import kotlinx.android.synthetic.main.activity_investment.*
import kotlinx.android.synthetic.main.content_investment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class InvestmentActivity : AppCompatActivity() {


    private val viewModel: InvestFormViewModel by viewModel()
    private val locale by lazy { Locale("pt", "BR") }
    private val confirmDialog by lazy { AlertDialog.Builder(this).create() }
    val didSucceedDialog by lazy { AlertDialog.Builder(this).create() }
    lateinit var pickerDialog: DatePickerDialog

    private val simulateClick: (View) -> Unit = {
        viewModel.getInvestmentSimulation(
            et_investment_amount.text.toString(),
            et_investment_maturity_date.text.toString(),
            et_cdi_index_title.text.toString()
        )
    }

    private val maturityDateClick: (View, MotionEvent) -> Boolean = { _, motionEvent ->
        (MotionEvent.ACTION_DOWN == motionEvent.action).apply {
            if (this) {
                val now = Calendar.getInstance()
                pickerDialog =
                    DatePickerDialog.newInstance(
                        { _, year, monthOfYear, dayOfMonth ->
                            et_investment_maturity_date.text = getString(
                                R.string.format, dayOfMonth, monthOfYear.plus(1), year
                            ).toEditable()
                        },
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                    )
                pickerDialog.minDate = now
                pickerDialog.show(supportFragmentManager, "tag_maturity_date")
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_investment)
        setupToolbar()
        setupLiveDataObservers()
        savedInstanceState?.let {
            viewModel.restore()
        } ?: run {
            viewModel.initInvestmentForm()
        }
    }

    private fun disableInputs() {
        et_investment_amount.isEnabled = false
        et_investment_maturity_date.isEnabled = false
        et_cdi_index_title.isEnabled = false
    }

    private fun disableLoading() {
        progress_view.visibility = View.GONE
    }

    private fun disableSimulateButton() {
        bt_simulate.isEnabled = false
    }

    private fun enableInput() {
        et_investment_amount.isEnabled = true
        et_investment_maturity_date.isEnabled = true
        et_cdi_index_title.isEnabled = true
    }

    private fun enableLoading() {
        progress_view.visibility = View.VISIBLE
    }

    private fun enableSimulateButton() {
        bt_simulate.isEnabled = true
    }

    private fun setFeedbackDialogParamsAndShow(
        @StringRes title: Int, @StringRes resMessage: Int, customMessage: String? = null
    ) {
        didSucceedDialog.setTitle(getString(title))
        val message = if (customMessage.isNullOrBlank()) getString(resMessage) else customMessage
        didSucceedDialog.setMessage(message)
        didSucceedDialog.setButton(
            DialogInterface.BUTTON_POSITIVE,
            getString(R.string.lbl_ok)
        ) { dialog, _ -> dialog.dismiss() }
        didSucceedDialog.show()
    }

    fun setupLiveDataObservers() {
        viewModel.stateInvestmentForm.observe(this, Observer {
            when (it) {
                is InitialInvestmentFormState -> showInitialInvestmentFormState()
                is InvalidFormState -> showInvalidFormState()
                is LoadingInvestmentFormState -> showLoadingInvestmentFormState()
                is CalculatedInvestmentFormState -> showCalculatedInvestmentFormState(it)
                is ErrorInvestmentFormState -> showErrorInvestmentFormState(it.errorMessage)
                is NetworkErrorInvestmentFormState -> showNetworkErrorInvestmentFormState(it.errorMessage)
            }
        })
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.setTitleTextColor(
            ContextCompat.getColor(
                applicationContext,
                R.color.material_white
            )
        )
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }


    private fun setupViews() {
        et_investment_amount.addTextChangedListener(MoneyTextWatcher(et_investment_amount, locale))
        et_cdi_index_title.addTextChangedListener(PercentTextWatcher(et_cdi_index_title))
        et_investment_maturity_date.setOnTouchListener(maturityDateClick)
        bt_simulate.setOnClickListener(simulateClick)
    }

    private fun showInitialInvestmentFormState() {
        disableLoading()
        setupViews()
    }

    private fun showCalculatedInvestmentFormState(it: CalculatedInvestmentFormState) {
        disableLoading()
        enableInput()
        enableSimulateButton()
        val intent =
            Intent(
                this,
                Class.forName("com.danilodequeiroz.easyinvestchallenge.investmentresult.ui.InvestmentResultActivity")
            )
        intent.putExtra(INVESTMENT_MODEL_PARAM, it.data)
        startActivity(intent)
    }

    private fun showErrorInvestmentFormState(errorMessage: Int) {
        setFeedbackDialogParamsAndShow(R.string.ops, errorMessage)
        disableLoading()
        enableInput()
        enableSimulateButton()
    }

    private fun showInvalidFormState() {
        Toast.makeText(this, R.string.invalid_fields, Toast.LENGTH_SHORT).show()
    }

    private fun showLoadingInvestmentFormState() {
        disableInputs()
        disableSimulateButton()
        enableLoading()
    }

    private fun showNetworkErrorInvestmentFormState(errorMessage: Int) {
        setFeedbackDialogParamsAndShow(R.string.ops, errorMessage)
        disableLoading()
        enableInput()
        enableSimulateButton()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return false
            }
        }
        return super.onOptionsItemSelected(item)
    }


}
