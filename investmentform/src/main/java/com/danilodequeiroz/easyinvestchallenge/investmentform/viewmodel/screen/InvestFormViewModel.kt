package com.danilodequeiroz.easyinvestchallenge.investmentform.viewmodel.screen

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.danilodequeiroz.easyinvestchallenge.common.API_DATE_PATTERN
import com.danilodequeiroz.easyinvestchallenge.common.DATE_PATTERN
import com.danilodequeiroz.easyinvestchallenge.common.ext.isNetworkError
import com.danilodequeiroz.easyinvestchallenge.investmentform.R
import com.danilodequeiroz.easyinvestchallenge.investmentform.domain.InvestmentFormInteractor
import com.danilodequeiroz.easyinvestchallenge.investmentform.presentation.*
import com.danilodequeiroz.easyinvestchallenge.common.interchangeable_viewmodel.InvestmentViewModel
import com.danilodequeiroz.easyinvestchallenge.investmentform.domain.viewModelDateToApiDate
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import org.apache.commons.validator.GenericValidator

class InvestFormViewModel constructor(
    val investmentFormInteractor: InvestmentFormInteractor,
    val subscribeOnScheduler: Scheduler,
    val observeOnScheduler: Scheduler
) : ViewModel() {

    val stateInvestmentForm = MutableLiveData<InvestmentFormState>()

    val disposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }

    fun initInvestmentForm() {
        stateInvestmentForm.value = InitialInvestmentFormState(null)
    }

    fun getInvestmentSimulation(rawInvestedAmount: String,
                                rawDate: String,
                                rawCdiIndex: String) {

        val investedAmount = investmentFormInteractor.stringBRLMoneyToDouble(rawInvestedAmount)
        val date = viewModelDateToApiDate(rawDate)
        val cdiIndex = investmentFormInteractor.stringPercentToDouble(rawCdiIndex)

        if(investedAmount <= 0 || !GenericValidator.isDate(date,API_DATE_PATTERN,true) || cdiIndex <= 0 ){
            stateInvestmentForm.value = InvalidFormState()
            return
        }
        stateInvestmentForm.value = LoadingInvestmentFormState()
        disposable.add(
            investmentFormInteractor.getInvestmentSimulation(investedAmount, date, cdiIndex)
                .subscribeOn(subscribeOnScheduler)
                .observeOn(observeOnScheduler)
                .subscribe({ onGetInvestmentSimulation(it) }, { t ->
                    stateInvestmentForm.value = if (t.isNetworkError()) {
                        NetworkErrorInvestmentFormState(
                            R.string.network_error_message,
                            null
                        )
                    } else {
                        ErrorInvestmentFormState(
                            R.string.technical_error_message,
                            null
                        )
                    }
                })

        )
    }

    private fun onGetInvestmentSimulation(it: InvestmentViewModel) {
        stateInvestmentForm.value =
            CalculatedInvestmentFormState(it)
    }

    fun restore() {
        stateInvestmentForm.value =
            InitialInvestmentFormState(
                stateInvestmentForm.value?.data
            )
    }


}
