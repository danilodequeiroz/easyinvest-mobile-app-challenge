package com.danilodequeiroz.easyinvestchallenge.investmentform.di

import com.danilodequeiroz.easyinvestchallenge.common.*
import com.danilodequeiroz.easyinvestchallenge.common.util.isRoboUnitTest
import com.danilodequeiroz.easyinvestchallenge.investmentform.domain.InvestmentFormInteractor
import com.danilodequeiroz.easyinvestchallenge.investmentform.viewmodel.screen.InvestFormViewModel
import com.danilodequeiroz.restapi.repository.InvestmentFormRepository
import com.danilodequeiroz.restapi.repository.mock.MockInvestmentFormRepositoryImpl
import com.danilodequeiroz.restapi.repository.real.InvestmentFormRepositoryImpl
import com.danilodequeiroz.restapi.service.InvestmentSimulateService
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val qualifierIO = named(SCHEDULER_SUBSCRIBE_ON_IO)
val qualifierUI = named(SCHEDULER_OBSERVE_ON_MAIN_THREAD)
val qualifierGson = named(GSON)
val qualifierOkHttpClient = named(OKHTTP3_CLIENT)
val qualifierRetrofit = named(RETROFIT)


val moduleInvestFormModule = module {


    viewModel {
        InvestFormViewModel(
            get(),
            get(qualifierIO),
            get(qualifierUI)
        )
    }

}

val moduleNetworkRepository = module {


    factory<Scheduler>(qualifierIO) { Schedulers.io() }

    factory<Scheduler>(qualifierUI) { AndroidSchedulers.mainThread() }

    factory {
        InvestmentFormInteractor(
            get()
        )
    }

    factory<InvestmentFormRepository> {
        if(!isRoboUnitTest()){
//            MockInvestmentFormRepositoryImpl()
            InvestmentFormRepositoryImpl(get())
        }else{
            MockInvestmentFormRepositoryImpl()
        }
    }

    factory <InvestmentSimulateService> {
        val retrofit : Retrofit by inject(qualifierRetrofit)
        retrofit.create(InvestmentSimulateService::class.java)
    }

    factory (qualifierRetrofit){
        Retrofit.Builder()
            .baseUrl("https://api-simulator-calc.easynvest.com.br/")
            .addConverterFactory(GsonConverterFactory.create(get(qualifierGson)))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(get(qualifierOkHttpClient))
            .build()
    }

    factory(qualifierGson) {
        GsonBuilder().create()
    }


    factory(qualifierOkHttpClient) {
        val client = OkHttpClient.Builder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .readTimeout(5, TimeUnit.SECONDS)
            .writeTimeout(5, TimeUnit.SECONDS)
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
        client.addNetworkInterceptor(StethoInterceptor())
        client.addInterceptor(httpLoggingInterceptor)
        client.build()
    }
}