package com.danilodequeiroz.easyinvestchallenge.investmentform.domain

import com.danilodequeiroz.easyinvestchallenge.common.interchangeable_viewmodel.InvestmentViewModel
import com.danilodequeiroz.restapi.repository.InvestmentFormRepository
import io.reactivex.Single
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

class InvestmentFormInteractor constructor(
    val investmentFormRepository: InvestmentFormRepository
) :
    PostInvestmentFormUseCase {

    private val locale by lazy { Locale("pt", "BR") }

    override fun stringBRLMoneyToDouble(value: String): Double {
        val replaceable =
            String.format("[%s,.\\$\\s]", NumberFormat.getCurrencyInstance(locale).currency?.symbol)
        val cleanString = value.replace(replaceable.toRegex(), "")
        return cleanString.toLong().toDouble() / 100
    }

    override fun stringPercentToDouble(value: String): Double {
        val locale = Locale("pt", "BR")
        val decimalSeparator =
            (NumberFormat.getPercentInstance(locale) as DecimalFormat).decimalFormatSymbols.decimalSeparator.toString()
        val cleanString = value.replace("[%]".toRegex(), "")
            .replace(decimalSeparator, ".")
        return cleanString.toDoubleOrNull() ?: 0.0
    }


    override fun getInvestmentSimulation(
        investedAmount: Double,
        date: String,
        cdiIndex: Double
    ): Single<InvestmentViewModel> {
        return investmentFormRepository.getInvestmentSimulation(investedAmount, date, cdiIndex)
            .map { response ->
                return@map response.toViewModel(cdiIndex)
            }
    }

}
