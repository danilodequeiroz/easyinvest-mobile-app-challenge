package com.danilodequeiroz.easyinvestchallenge.investmentform.domain

import com.danilodequeiroz.easyinvestchallenge.common.interchangeable_viewmodel.InvestmentViewModel
import io.reactivex.Single

interface PostInvestmentFormUseCase {
    fun getInvestmentSimulation(investedAmount: Double,
                                date: String,
                                cdiIndex: Double) : Single<InvestmentViewModel>

    fun stringBRLMoneyToDouble(value : String) : Double
    fun stringPercentToDouble(value : String) : Double
}


