package com.danilodequeiroz.easyinvestchallenge.investmentform.presentation

import androidx.annotation.StringRes
import com.danilodequeiroz.easyinvestchallenge.common.interchangeable_viewmodel.InvestmentViewModel


sealed class InvestmentFormState { abstract val data: InvestmentViewModel? }

data class InitialInvestmentFormState(override val data: InvestmentViewModel? = null) : InvestmentFormState()
data class InvalidFormState(override val data: InvestmentViewModel? = null) : InvestmentFormState()
data class LoadingInvestmentFormState(override val data: InvestmentViewModel? = null) : InvestmentFormState()
data class CalculatedInvestmentFormState(override val data: InvestmentViewModel?) : InvestmentFormState()
data class ErrorInvestmentFormState(@StringRes val errorMessage: Int, override val data: InvestmentViewModel? = null) : InvestmentFormState()
data class NetworkErrorInvestmentFormState(@StringRes val errorMessage: Int, override val data: InvestmentViewModel? = null) : InvestmentFormState()
