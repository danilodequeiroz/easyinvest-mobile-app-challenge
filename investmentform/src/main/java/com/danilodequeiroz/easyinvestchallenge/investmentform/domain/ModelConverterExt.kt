package com.danilodequeiroz.easyinvestchallenge.investmentform.domain

import android.annotation.SuppressLint
import com.danilodequeiroz.easyinvestchallenge.common.API_DATE_PATTERN
import com.danilodequeiroz.easyinvestchallenge.common.DATE_PATTERN
import com.danilodequeiroz.easyinvestchallenge.common.ext.toBRLMoneyString
import com.danilodequeiroz.easyinvestchallenge.common.ext.toPercentString
import com.danilodequeiroz.easyinvestchallenge.common.interchangeable_viewmodel.InvestmentViewModel
import com.danilodequeiroz.restapi.model.Investment
import org.apache.commons.validator.GenericValidator
import java.text.SimpleDateFormat

fun Investment.toViewModel(cdiIndex: Double): InvestmentViewModel {
    val response = this
    val investmentViewModel = InvestmentViewModel(
        investedAmount = response.investmentParameter?.investedAmount?.toBRLMoneyString(),
        grossAmount = response.grossAmount?.toBRLMoneyString(),
        grossAmountProfit = response.grossAmountProfit?.toBRLMoneyString(),
        taxesRate = "${response.taxesAmount?.toBRLMoneyString()} (${response.taxesRate?.toPercentString()})",
        netAmountProfit = response.netAmountProfit?.toBRLMoneyString(),
        maturityDate = apiDateToViewModelDate(response.investmentParameter?.maturityDate),
        maturityTotalDays = response.investmentParameter?.maturityTotalDays,
        monthlyGrossRateProfit = response.monthlyGrossRateProfit?.toPercentString(),
        cdiIndex = cdiIndex.toPercentString(),
        yearlyInterestRate = response.investmentParameter?.yearlyInterestRate?.toPercentString(),
        rateProfit = response.rateProfit?.toPercentString()
    )
    return investmentViewModel
}



@SuppressLint("SimpleDateFormat")
fun apiDateToViewModelDate(apiDate: String?): String {
    var stringNewDate : String = ""
    apiDate?.let {
        val simpleFormatFromDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss") // 2023-03-03T00:00:00
        val simpleFormatNewDate = SimpleDateFormat("dd/MM/yyyy")
        val tempDate = simpleFormatFromDate.parse(apiDate)
        stringNewDate = if(tempDate != null ) simpleFormatNewDate.format(tempDate) else ""
    }
    return stringNewDate
}

@SuppressLint("SimpleDateFormat")
fun viewModelDateToApiDate(date: String?): String {
    var stringNewDate : String = ""
    if(GenericValidator.isDate(date, DATE_PATTERN,true)){
        date?.let {
            val simpleFormatFromDate = SimpleDateFormat("dd/MM/yyyy") // 2023-03-03T00:00:00
            val simpleFormatNewDate = SimpleDateFormat("yyyy-MM-dd")
            val tempDate = simpleFormatFromDate.parse(date)
            stringNewDate = if(tempDate != null ) simpleFormatNewDate.format(tempDate) else ""
        }
    }
    return stringNewDate
}