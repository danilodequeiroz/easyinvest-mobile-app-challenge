package com.danilodequeiroz.restapi.repository

import com.danilodequeiroz.restapi.model.Investment
import io.reactivex.Single

interface InvestmentFormRepository {
    fun getInvestmentSimulation(
        investedAmount: Double,
        date: String,
        cdiIndex: Double
    ): Single<Investment>
}