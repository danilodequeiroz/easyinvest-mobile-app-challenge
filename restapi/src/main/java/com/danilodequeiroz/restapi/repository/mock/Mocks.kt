package com.danilodequeiroz.restapi.repository.mock

import com.danilodequeiroz.restapi.model.Investment
import com.google.gson.Gson
import com.google.gson.JsonObject



class Mocks {


    val investmentObject = Gson().fromJson(rawInvestmentJson, Investment::class.java)
}


const val rawInvestmentJson =
    "    {\n" +
            "        \"investmentParameter\": {\n" +
            "        \"investedAmount\": 32323.0,\n" +
            "        \"yearlyInterestRate\": 9.5512,\n" +
            "        \"maturityTotalDays\": 1981,\n" +
            "        \"maturityBusinessDays\": 1409,\n" +
            "        \"maturityDate\": \"2023-03-03T00:00:00\",\n" +
            "        \"rate\": 123.0,\n" +
            "        \"isTaxFree\": false\n" +
            "    },\n" +
            "        \"grossAmount\": 60528.20,\n" +
            "        \"taxesAmount\": 4230.78,\n" +
            "        \"netAmount\": 56297.42,\n" +
            "        \"grossAmountProfit\": 28205.20,\n" +
            "        \"netAmountProfit\": 23974.42,\n" +
            "        \"annualGrossRateProfit\": 87.26,\n" +
            "        \"monthlyGrossRateProfit\": 0.76,\n" +
            "        \"dailyGrossRateProfit\": 0.000445330025305748,\n" +
            "        \"taxesRate\": 15.0, \n" +
            "        \"rateProfit\": 9.5512, \n" +
            "        \"annualNetRateProfit\": 74.17\n" +
            "    }"