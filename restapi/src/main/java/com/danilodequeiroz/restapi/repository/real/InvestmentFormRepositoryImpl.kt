package com.danilodequeiroz.restapi.repository.real

import com.danilodequeiroz.restapi.model.Investment
import com.danilodequeiroz.restapi.repository.InvestmentFormRepository
import com.danilodequeiroz.restapi.service.InvestmentSimulateService
import io.reactivex.Single

class InvestmentFormRepositoryImpl(val investmentSimulateService: InvestmentSimulateService) : InvestmentFormRepository {

    override fun getInvestmentSimulation(investedAmount: Double, date: String, cdiIndex: Double): Single<Investment> =
        investmentSimulateService.getInvestmentSimulation(investedAmount, date, cdiIndex)

}