package com.danilodequeiroz.restapi.repository.mock

import com.danilodequeiroz.restapi.model.Investment
import com.danilodequeiroz.restapi.repository.InvestmentFormRepository
import io.reactivex.Single
import java.util.concurrent.TimeUnit


class MockInvestmentFormRepositoryImpl : InvestmentFormRepository {

    override fun getInvestmentSimulation(
        investedAmount: Double,
        date: String,
        cdiIndex: Double
    ): Single<Investment> {
        return Single.just(Mocks().investmentObject)
//        return Single.just(Mocks().investmentObject).delay(5,TimeUnit.SECONDS)
    }

}