package com.danilodequeiroz.restapi.service

import com.danilodequeiroz.restapi.model.Investment
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface InvestmentSimulateService {
    @GET("calculator/simulate")
    fun getInvestmentSimulation(
        @Query("investedAmount") investedAmount: Double,
        @Query("maturityDate") date: String,
        @Query("rate") cdiIndex: Double,
        @Query("isTaxFree") isTaxFree: Boolean = false,
        @Query("index") index: String = "CDI"
    ): Single<Investment>
}
