package com.danilodequeiroz.restapi.model

import com.google.gson.annotations.SerializedName

data class InvestmentParameter(

	@field:SerializedName("rate")
	val rate: Double? = null,

	@field:SerializedName("maturityDate")
	val maturityDate: String? = null,

	@field:SerializedName("maturityBusinessDays")
	val maturityBusinessDays: Int? = null,

	@field:SerializedName("investedAmount")
	val investedAmount: Double? = null,

	@field:SerializedName("maturityTotalDays")
	val maturityTotalDays: Int? = null,

	@field:SerializedName("isTaxFree")
	val isTaxFree: Boolean? = null,

	@field:SerializedName("yearlyInterestRate")
	val yearlyInterestRate: Double? = null
)