package com.danilodequeiroz.restapi.model

import com.google.gson.annotations.SerializedName

data class Investment(

	@field:SerializedName("dailyGrossRateProfit")
	val dailyGrossRateProfit: Double? = null,

	@field:SerializedName("investmentParameter")
	val investmentParameter: InvestmentParameter? = null,

	@field:SerializedName("monthlyGrossRateProfit")
	val monthlyGrossRateProfit: Double? = null,

	@field:SerializedName("netAmount")
	val netAmount: Double? = null,

	@field:SerializedName("grossAmountProfit")
	val grossAmountProfit: Double? = null,

	@field:SerializedName("annualGrossRateProfit")
	val annualGrossRateProfit: Double? = null,

	@field:SerializedName("taxesAmount")
	val taxesAmount: Double? = null,

	@field:SerializedName("netAmountProfit")
	val netAmountProfit: Double? = null,

	@field:SerializedName("rateProfit")
	val rateProfit: Double? = null,

	@field:SerializedName("grossAmount")
	val grossAmount: Double? = null,

	@field:SerializedName("taxesRate")
	val taxesRate: Double? = null,

	@field:SerializedName("annualNetRateProfit")
	val annualNetRateProfit: Double? = null
)